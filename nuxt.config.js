import { gql_product, headers, gql } from "./helpers/gql_partials";
import fetch from "node-fetch";

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',
  ssr: true,

  loading: {
    color: '#8a2be2',
    height: '3px'
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'piko made',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: 'https://cdn.shopify.com/s/files/1/0595/2841/3376/files/favicon.png?v=1629887157' }
    ]
  },

  pageTransition: 'fade',

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/reset.css',
    '@/assets/css/bootstrap.css',
    '@/assets/css/main.css',
    '@/assets/css/slick.css'
  ],

  buildModules: [
    '@nuxtjs/google-analytics'
  ],

  googleAnalytics: {
    id: 'UA-206608235-1'
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/vue-js-modal.js',
    // { src: '~/plugins/persistedState.client.js', mode: 'client' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  generate: {
    async routes() {
      return fetch(gql, { 
        method: "POST", 
        headers,
        body: `
          {
            products(first: 100) {
              edges {
                node ${gql_product}
              }
            }
          }
        ` 
      })
      .then(res => res.json())
      .then(json => {
        return json.data.products.edges.map(({node:product}) => {
          return {
            route: `/products/${product.handle}`,
            payload: product
          }
        })
      })
    }
  }

}
