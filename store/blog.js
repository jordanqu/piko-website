import { gql, gql_posts, gql_posts_by_handle, headers } from "~/helpers/gql_partials_contentful";

export const state = () => ({
  posts:[]
});

export const mutations = {
  SET_POSTS(state, posts) {
    state.posts = posts;
  },
}

export const actions = {
  async get_posts({commit}) {
    try {
      const res = await fetch(gql, { 
        method: "POST", 
        headers,
        body: JSON.stringify({
          query: gql_posts
        })
      })
      .then(res => res.json());
      const posts = res.data.blogPostCollection.items;
      commit("SET_POSTS", posts);
      return posts;
    }
    catch(error) {
      return [];
    }
  },
  async get_post_by_handle({state}, handle) {
    let post;
    post = state.posts.find(post => post.handle === handle);
    if ( post ) {
      return post;
    } 
    else {
      const res = await fetch(gql, { 
        method: "POST", 
        headers,
        body: JSON.stringify({
          query: gql_posts_by_handle(handle)
        })
      })
      .then(res => res.json());
      if (res.data.blogPostCollection.items.length > 0) {
        return res.data.blogPostCollection.items[0]
      }
      else {
        return {}
      }
    }
  }
}