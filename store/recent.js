export const state = () => ({
  show_recents: true,
  recent_orders: []
});

export const mutations = {
  SET_SHOW_RECENTS(state, bool) {
    state.show_recents = bool;
  },
  SET_RECENT_ORDERS(state, orders) {
    state.recent_orders = orders;
  }
}

export const actions = {
  async get_recents({commit}) {
    try {
      let recents = await fetch("https://ap-southeast-2.aws.webhooks.mongodb-realm.com/api/client/v2.0/app/piko-made-jbnjo/service/HTTP/incoming_webhook/recent_orders").then(res => res.json())
      commit("SET_RECENT_ORDERS", recents);
    }
    catch(err) {
      commit("SET_RECENT_ORDERS", []);
    }
  }
}