import { product_map } from "~/helpers/mappers";
import { gql, gql_product_search, headers } from "~/helpers/gql_partials";

export const state = () => ({
});

export const actions = {
  async perform(_, term) {
    try {
      const search = await fetch(gql, { 
        method: "POST", 
        headers,
        body: `${gql_product_search(term)}` 
      })
      .then(res => res.json());
      return product_map(search.data.products.edges);
    }
    catch(error) {
      return [];
    }
  }
}