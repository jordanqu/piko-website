import { product_map } from "~/helpers/mappers";
import { gql, headers, gql_checkout, gql_collection, gql_init_store, gql_product_by_handle } from "~/helpers/gql_partials";


export const state = () => ({
  store: {},
  products: [],
  collections: [],
  reviews: {data:[]},
  collections_page_info: {},
  checkout: {},
  customer: {
    country: ""
  }
});

export const mutations = {
  SET_STORE(state, store) {
    state.store = store;
  },
  SET_CUSTOMER_CC(state, cc) {
    state.customer.country = cc;
  },
  SET_PRODUCTS(state, products) {
    state.products = products;
  },
  SET_CHECKOUT(state, checkout) {
    state.checkout = checkout;
  },
  SET_COLLECTIONS(state, collections) {
    state.collections = collections.sort((a, b) => a.node.title.localeCompare(b.node.title));
  },
  SET_COLLECTION_PRODUCTS(state, {handle, products}) {
    let collection = state.collections.find(({node:collection}) => collection.handle === handle);
    if (collection) collection.node.products = products;
  },
  SET_COLLECTION_PAGE_INFO(state, info) {
    state.collections_page_info = info;
  },
  SET_REVIEWS(state, reviews) {
    state.reviews = reviews;
  }
}

export const actions = {
  async nuxtServerInit({dispatch}) {
    await dispatch("init_store");
  },
  async init_store({commit}) {
    const shop = await fetch(gql, { 
      method: "POST", 
      headers,
      body: `${gql_init_store}` 
    })
    .then(res => res.json());
    commit('SET_STORE', shop.data.shop);
    commit('SET_COLLECTIONS', shop.data.collections.edges);
  },
  async create_checkout({commit}) {
    try {
      const checkout = await fetch(gql, { 
        method: "POST", 
        headers,
        body: `
        mutation {
          checkoutCreate(input: {}) 
          ${gql_checkout}
        }`
      })
      .then(res => res.json());
      commit('SET_CHECKOUT', checkout.data.checkoutCreate.checkout);
    }
    catch(err) {
      commit('SET_CHECKOUT', {});
    }
  },
  async collection_by_handle({commit, state}, {handle, cursor, position}) {
    // let existing_col = state.collections.find(({node:collection}) => collection.handle === handle);
    // if ( existing_col && existing_col.node.products ) {
    //   return { success: true, products: existing_col.node.products };
    // }
    try {
      const body = gql_collection(handle, cursor, position);
      const collection = await fetch(gql, { 
        method: "POST", 
        headers,
        body
      })
      .then(res => res.json());
      let products = product_map(collection.data.collectionByHandle.products.edges);
      commit("SET_COLLECTION_PRODUCTS", {handle, products});
      commit("SET_COLLECTION_PAGE_INFO", collection.data.collectionByHandle.products.pageInfo);
      return { success: true, products };
    }
    catch({message}) {
      return { success: false , message };
    }
  },
  async product_by_handle({commit, state}, handle) {
    let existing_product = state.products.find(({node:product}) => product.handle === handle);
    if ( existing_product ) {
      return { success: true, product: existing_product };
    }
    try {
      const body = gql_product_by_handle(handle);
      const product = await fetch(gql, { 
        method: "POST", 
        headers,
        body
      })
      .then(res => res.json());
      let new_product = product_map(product.data.productByHandle);
      let products = state.products.filter(product => product.handle !== handle);
      products.push(new_product);
      commit("SET_PRODUCTS", products);
      return { success: true, product:new_product };
    }
    catch({message}) {
      return { success: false , message };
    }
  },
  async get_country({commit}) {
    try {
      const cc = await fetch("https://d3v4l4ufews1y5.cloudfront.net/")
      .then(res => res.json())
      .then(json => json.country_code)
      commit("SET_CUSTOMER_CC", cc);
    }
    catch (err) {
      commit("SET_CUSTOMER_CC", "");
    }
  },
  async get_reviews({commit}) {
    try {
      const reviews = await fetch("https://stamped.io/api/widget/reviews?&page=1&storeUrl=pikomade.com&apiKey=pubkey-6UkoO4B9M3R347h2I6M55QkY9Kwd2C").then(res => res.json());
      commit("SET_REVIEWS", reviews);
    } 
    catch(err) {
      commit("SET_REVIEWS", {data:[]});
    }
  },
  async review_vote({dispatch}, id) {
    const res = await fetch("https://stamped.io/api/reviews/vote", {
      method: "POST",
      headers: {
        "content-type": "application/json"
      },
      body: JSON.stringify({
        reviewId: id,
        vote: 1,
        sId: "226109",
        apiKey: "pubkey-6UkoO4B9M3R347h2I6M55QkY9Kwd2C"
      })
    })
    .then(res => res.json());
    dispatch("get_reviews")
  }
}


export const getters = {
  country_code(state) {
    return state.customer.country ? state.customer.country : "";
  }
}