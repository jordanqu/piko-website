import { gql, headers, gql_cart } from "~/helpers/gql_partials";

export const state = () => ({
  cart: {},
  shown: false
});

export const mutations = {
  SET(state, cart) {
    state.cart = cart;
  },
  SET_SHOWN(state, bool) {
    state.shown = bool;
  },
}

export const actions = {
  async create({commit}) {
    try {
      const cart = await fetch(gql, {
        method: "POST", 
        headers,
        body: `
          mutation {
            cartCreate(input: {}) {
              ${gql_cart}
            }
          }
        `
      })
      .then(res => res.json());
      commit('SET', cart.data.cartCreate.cart);
    }
    catch(err) {
      commit('SET', {});
    }
  },
  async add({commit, state}, id) {
    const cart = await fetch(gql, {
      method: "POST", 
      headers,
      body: `
        mutation {
          cartLinesAdd(lines:[{merchandiseId: "${id}"}], cartId:"${state.cart.id}") {
            ${gql_cart}
          }
        }
      `
    })
    .then(res => res.json());
    commit("SET", cart.data.cartLinesAdd.cart);
    commit("SET_SHOWN", true);
    return;
  },
  async remove({commit, state}, id) {
    const cart = await fetch(gql, {
      method: "POST", 
      headers,
      body: `
        mutation {
          cartLinesRemove(lineIds:["${id}"], cartId:"${state.cart.id}") {
            ${gql_cart}
          }
        }
      `
    })
    .then(res => res.json());
    commit('SET', cart.data.cartLinesRemove.cart);
    return;
  },
  async update({commit, state}, {id, qty}) {
    const cart = await fetch(gql, {
      method: "POST", 
      headers,
      body: `
        mutation {
          cartLinesUpdate(cartId:"${state.cart.id}", lines:[{id:"${id}", quantity:${qty}}]) {
            ${gql_cart}
          }
        }
      `
    })
    .then(res => res.json());
    commit('SET', cart.data.cartLinesUpdate.cart);
    return;
  },
  async apply_discount({commit, state}, code) {
    const cart = await fetch(gql, {
      method: "POST", 
      headers,
      body: `
        mutation {
          cartDiscountCodesUpdate(cartId:"${state.cart.id}", discountCodes:["${code}"]) {
            ${gql_cart}
          }
        }
      `
    })
    .then(res => res.json());
    commit("SET", cart.data.cartDiscountCodesUpdate.cart);
    return;
  }
}


export const getters = {
  items(state) {
    if ( state.cart && state.cart.lines ) {
      return state.cart.lines.edges.reduce((a, { node }) => {
        return a + node.quantity
      }, 0);
    }
    else {
      0
    }
  }
}