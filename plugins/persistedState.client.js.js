import createPersistedState from 'vuex-persistedstate'

export default ({store}) => {
  createPersistedState({
    key: "checkout",
    paths: ["checkout"]
  })(store)
}