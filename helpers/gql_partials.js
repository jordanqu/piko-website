export const headers = { 
  "Content-Type": "application/graphql", 
  "X-Shopify-Storefront-Access-Token": "abab527a75ba107baf2e8f23606a405b"
};
export const store = "piko-made.myshopify.com";


export const gql = `https://${store}/api/2021-10/graphql.json`;

export const gql_product = `
  {
    id
    title
    description
    descriptionHtml
    tags
    handle
    totalInventory
    productType
    createdAt
    availableForSale
    collections(first:1) {
      edges {
        node {
          title
          handle
        }
      }
    }
    priceRange {
      minVariantPrice {
        amount
      }
    }
    metafields(first:5) {
      edges {
        node {
          id
          key
          value
        }
      }
    }
    variants(first: 20) {
      edges {
        node {
          id
          availableForSale
          sku
          title
          quantityAvailable
          image {
            id
            altText
            transformedSrc
          }
          priceV2 {
            amount
            currencyCode
          }
          compareAtPriceV2 {
            amount
          }
        }
      }
    }
    images(first: 10) {
      edges {
        node {
          id
          altText
          transformedSrc
        }
      }
    }
  }
`

export const gql_init_store = `
  {
    shop {
      name
      description
      currencyCode
    }
    collections(first: 100) {
      edges {
        node {
          id
          title
          handle
        }
      }
    }
    products(first: 35) {
      edges {
        node ${gql_product}
      }
    }
  }
`;



export const gql_cart = `
  cart {
    id
    checkoutUrl
    estimatedCost {
      subtotalAmount {
        amount
        currencyCode
      }
      totalAmount {
        amount
        currencyCode
      }
    }
    lines(first:50) {
      edges {
        node {
          id
          quantity
          merchandise {
            ... on ProductVariant {
              id
              availableForSale
              sku
              title
              quantityAvailable
              product {
                title
              }
              image {
                id
                altText
                transformedSrc
              }
              priceV2 {
                amount
                currencyCode
              }
              compareAtPriceV2 {
                amount
              }
            }
          }
          discountAllocations {
            discountedAmount {
              amount
              currencyCode
            }
          }
          estimatedCost {
            subtotalAmount {
              amount
              currencyCode
            }
            totalAmount {
              amount
              currencyCode
            }
          }
        }
      }
    }
  }
  userErrors {
    code
    field
    message
  }
`;


export const gql_checkout = `{
  checkout {
    id
    webUrl
    email
    lineItemsSubtotalPrice {
      amount
      currencyCode
    }
    totalPriceV2 {
      amount
      currencyCode
    }
    lineItems(first: 30) {
      edges {
        node {
          id
          title
          quantity
          variant {
            id
            title
            quantityAvailable
            priceV2 {
              amount
              currencyCode
            }
            image {
              id
              altText
              transformedSrc
            }
          }
        }
      }
    }
  }
}`;

export const gql_product_by_handle = (handle) => {
  return `{
      productByHandle(handle: "${handle}") 
      ${gql_product}
    }
  `  
};

export const gql_collection = (handle, cursor=null, position=null) => {
  let query = `first:18`;
  if ( cursor && position ) {
    if ( position ===  "after" ) {
      query = `first:18${ cursor ? `,after:"${cursor}"` : "" }`;
    }
    if ( position ===  "before" ) {
      query = `last:18${ cursor ? `,before:"${cursor}"` : "" }`;
    }
  }
  return `
    query {
      collectionByHandle(handle: "${handle}") {
        products(${query}) {
          pageInfo {
            hasNextPage
            hasPreviousPage
          }
          edges {
            cursor
            node ${gql_product}
          }
        }
      }
    }
  `
}

export const gql_product_search = (query) => {
  return `
  {
    products(query:"title:*${query}* OR tag:*${query}*" first:10 ) {
      edges {
        node ${gql_product}
      }
    } 
  }
  `  
};