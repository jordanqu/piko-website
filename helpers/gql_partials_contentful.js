
export const headers = { 
  "Content-Type": "application/json", 
  "Authorization": "Bearer 2gwKM4f6r3_Frn-JCS_Z780LwJCoUK92QIGpeTQ0_7s"
};

export const gql = `https://graphql.contentful.com/content/v1/spaces/upqn5up0ny44`;

export const gql_post = `
  {
    items {
      title
      publishDate
      handle
      author
      shortDescription
      body {
        json
      }
      featureImage {
        title
        fileName
        url (transform: { width: 1920 })
      }
      galleryCollection {
        items {
          fileName
          url (transform: { width: 1920 })
          title
        }
      }
    }
  }
`;

export const gql_posts = `
  {
    blogPostCollection(limit:20, order:publishDate_DESC) ${gql_post}
  }
`;


export const gql_posts_by_handle = (handle) => {
  return `
    {
      blogPostCollection(where:{handle:"${handle}"}) ${gql_post}
    }
  `
};