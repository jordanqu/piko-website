export const product_map = ( products ) => {
    if ( Array.isArray(products) ) {
        let new_products = products.map(product => {
            let hasVariants = product.node.variants.edges.length > 1 ? true : false;
            if ( hasVariants ) {
                const new_images = [];
                let maxDiscount = 0;
                product.node.variants.edges.forEach((variant) => {
                    new_images.push({node:variant.node.image});
                    let discount_pc;
                    if ( variant.node.compareAtPriceV2 ) {
                        let compare_price = parseFloat(variant.node.compareAtPriceV2.amount);
                        let price = parseFloat(variant.node.priceV2.amount)
                        discount_pc =  ((compare_price - price)/compare_price)*100;
                        if ( discount_pc > maxDiscount ) maxDiscount = parseInt(discount_pc);
                    }
                });
                const repl = [...product.node.images.edges, ...new_images];
                const unique_images = [...new Map(repl.map(item => [item.node.id, item])).values()];
                product.node.images.edges = unique_images;
                product.node.maxDiscount = maxDiscount;
            }
            product.node.short_desc = `${product.node.description.split(" ").slice(0, 15).join(" ")}...`;
            product.node.hasVariants = hasVariants;
            product.node.fromPrice = parseFloat(product.node.priceRange.minVariantPrice.amount).toFixed(2);
            return product;
        });
        return new_products;
    }
    else {
        const product = {};
        product.node = products;
        let hasVariants = product.node.variants.edges.length > 1 ? true : false;
        if ( hasVariants ) {
            const new_images = [];
            let maxDiscount = 0;
            product.node.variants.edges.forEach((variant) => {
                let discount_pc;
                if ( variant.node.compareAtPriceV2 ) {
                    let compare_price = parseFloat(variant.node.compareAtPriceV2.amount);
                    let price = parseFloat(variant.node.priceV2.amount)
                    discount_pc =  ((compare_price - price)/compare_price)*100;
                    if ( discount_pc > maxDiscount ) maxDiscount = parseInt(discount_pc);
                }
                new_images.push({node:variant.node.image});
            });
            const repl = [...product.node.images.edges, ...new_images];
            const unique_images = [...new Map(repl.map(item => [item.node.id, item])).values()];
            product.node.images.edges = unique_images;
            product.node.maxDiscount = maxDiscount;
        }
        product.node.short_desc = `${product.node.description.split(" ").slice(0, 15).join(" ")}...`;
        product.node.hasVariants = hasVariants;
        product.node.fromPrice = parseFloat(product.node.priceRange.minVariantPrice.amount).toFixed(2);
        return product;
    }
}